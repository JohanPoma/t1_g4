package com.example.T1_GRUPO;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Controlador {
    
    //http://localhost
    //http://localhost/
    @GetMapping("/")
    public String principal()
    {
        return "index"; //index.html
    }
    
    //http://localhost/app
    @GetMapping("/sistem")
    public String Aplicaciones()
    {
        return "sistemas"; //aplicaciones.html
    }
    
    //http://localhost/music
    @GetMapping("/derecho")
    public String Musica()
    {
        return "musica"; //musica.html
    }
    
    //http://localhost/movie
    @GetMapping("/indrust")
    public String Pelicula()
    {
        return "peliculas"; //peliculas.html
    }
}