package com.example.T1_GRUPO;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T1GrupoApplication {

	public static void main(String[] args) {
		SpringApplication.run(T1GrupoApplication.class, args);
	}

}
